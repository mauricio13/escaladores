import util.BitManipulation;
import java.util.Random;

public class HillClimber {
	
	private Random rand;
	
	public HillClimber(){
		rand = new Random();
	}
	
	// The target function we are trying to optimize
	public float deJong(float args[]){
		float acc = 0;
		for (float num : args)
			acc += num*num;
		return acc;
	}
	


    public  float[] randomAscent(int args[], int iters){
        float fArg1 = findMaxRandomAscent(args,0,iters);
        float fArg2 = findMaxRandomAscent(args,1,iters);
        float fArg3 = findMaxRandomAscent(args,2,iters);
        float fArg4 = findMaxRandomAscent(args,3,iters);
        float retArgs[] = new float[4];
        retArgs[0] = fArg1;
        retArgs[1] = fArg2;
        retArgs[2] = fArg3;
        retArgs[3] = fArg4;
        return retArgs;
        
    }
    
    public  float[] nextAscent(int args[], int iters){
        float fArg1 =  findMaxNextAscent(args,0,iters);
        float fArg2 =  findMaxNextAscent(args,1,iters);
        float fArg3 =  findMaxNextAscent(args,2,iters);
        float fArg4 =  findMaxNextAscent(args,3,iters);
        float retArgs[] = new float[4];
        retArgs[0] = fArg1;
        retArgs[1] = fArg2;
        retArgs[2] = fArg3;
        retArgs[3] = fArg4;
        return retArgs;
        
    }
    

    
    // Find the best result (max value) for a given function (deJong function in this case)
    // modifying only one argument (cuerrentArgument)
    public float findMaxRandomAscent(int args[], int currentArgument, int numIterations) {
    	
    	// Set the start point to a minimum
        float actMax = 0;
        // Get the argument that will be modified
        int arg = args[currentArgument];
        // Get the float representation of the arguments
        float floatArgs[] = toFloat(args);
        
        
        for (int i = 0; i < numIterations; i++) {
            short index = (short) ((short)rand.nextInt(29) + 3);
            // Modify a random bit of the argument that we are modifying
            arg = BitManipulation.flip(args[currentArgument],index);
            // update our float arguments
            floatArgs[currentArgument] = BitManipulation.javaFloat(arg);
            // compute deJong function
            float deJongResult = deJong(floatArgs); 
            if (deJongResult> actMax) {
            	// If this is a better choice update the max and the argument that maxed that out
                actMax = deJongResult;
                args[currentArgument] = arg;
            }
        }
        
        // Return the value of the argument that maxed the function
        return  BitManipulation.javaFloat(args[currentArgument]);
    }
    
    public  float findMaxNextAscent(int args[], int currentArgument, int numIterations) {
    	
        double actMax = 0;
        
        int arg = args[currentArgument];
        float floatArgs[] = toFloat(args);
        short i = 0;
        
        for (int j = 1; j < numIterations; j++) {
            i = (short) ((i % 30) + 3);
            arg = BitManipulation.flip(arg, i);
            i++;
            floatArgs[currentArgument] = BitManipulation.javaFloat(arg);
            float deJongResult = deJong(floatArgs); 
            if (deJongResult > actMax) {
                actMax = deJongResult;
                args[currentArgument] = arg;
                i = 0;
            }
        }

        return  BitManipulation.javaFloat(args[currentArgument]);
    }

    
    public static float[] toFloat(int args[]){
    	float floatArgs[] = new float[args.length];
    	for (int i = 0; i<floatArgs.length; i++) floatArgs[i] = BitManipulation.javaFloat(args[i]);
    	return floatArgs;
    }
    


	
	public void printArrayOfFour(float args[]){
		System.out.println("{"+args[0]+", "+args[1]+", "+args[2]+", "+args[3]+"}");
	}
	
}
