package util;



public class BitManipulation {

	public static int leftmostBits(int x, int n){
		return 0;
	}
	
	public static int rightBits(int x, int n){
		return 1;
	}
	
	// returns the ith bit of the integer x
	public static int ith(int x, short ith){
		int mask = 1 << ith;
		x &= mask;
		x = x >>> ith;
		return x;
	}
	
	// Extracts the integer value from our floating point representation
	public static int integer(int x){
		// We need to return only the first 4 bits of the bitstring
		// everything else is fractional
		int sign = 1;
		int negative = ((1 << 31) & x);
		negative = negative >>> 31;
		if(negative > 0) sign *=-1;
		x = x << 1;
		return (x >>> 29) * sign;
	}
	
	// Extracts the fractional value from our floating point representation
	public static float fractional(int x){
		float fractional = 0;
		int mask = 1 << 27;
		int power = 2;
		for(int i = 0; i < 26; i++){
			int cur = (x & mask) >>> 27;
			fractional += (float)cur/(float)power;
			x = x << 1;
			power = power << 1;
			
		}
		return fractional;
	}
	
	// Transforms our custom floting point bit representation to the JAVA representation
	// we need to transform our numbers to java numbers in order to operate with them
	public static float javaFloat(int x){
		int integer = integer(x);
		float fractional = fractional(x);
		if(integer >= 0) return integer + fractional;
		return integer - fractional;
	}
	
	// flips the bit at position ith in the bitstring x
	public static int flip(int x, short ith){
		int ithVal = (1 << ith);
		int clearBit = ~ithVal;
		ithVal &= ~x;
		x = x & clearBit;
		return x | ithVal;
	}
}
