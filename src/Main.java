import java.util.ArrayList;

import util.BitManipulation;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		
		
		HillClimber hc = new HillClimber();
		int intArgs[] = {4,5,4,2};
		
		//float max[] = hc.randomAscent(intArgs,50);
		//System.out.println("The results that maxed the function using random ascent are");
		//for (float f : max) 
			//System.out.println(f);
		
		//System.out.println("With a value of");
		//System.out.println(hc.deJong(max));
		
		//System.out.println("*********************");
		
		float max2[] = hc.nextAscent(intArgs,5000);
		//System.out.println("The results that maxed the function using next ascent are");
		//for (float f : max2) 
		//	System.out.println(f);
		
		//System.out.println("With a value of");
		//System.out.println(hc.deJong(max2));
		
		//String boolExp = "1^0|0|1^1|1";
		//int v = validParenthesis(boolExp, true, new ArrayList<Boolean>());
		//System.out.println(v);
		
	
		test(50,hc,intArgs);
	}
	
	public static void test(int n, HillClimber hc, int args[]){
		for (int i = 0; i<n; i++){
			int argss[] = new int[4];
			argss[0] = 1;
			argss[1] = 1;
			argss[2] = 1;
			argss[3] = 1;
			float max[] = hc.randomAscent(argss, 50);
			System.out.println(hc.deJong(max));
		}
	}

}
